Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Guy St.C. Slater <guy.slater@gmail.com>
 The following have contributed code to this project:
 (in chronological order)
    Ewan Birney      <birney-AT-ebi.ac.uk>
    Ian Holmes       <ihh-AT-fruitfly.org>
    Steve Searle     <searle-AT-sanger.ac.uk>
    Tim Cutts        <tjrc-AT-sanger.ac.uk>
    Don Gilbert      <gilbertd-AT-indiana.edu>
    Michael Schuster <michaels-AT-ebi.ac.uk>
Source: https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate
Upstream-Name: Exonerate

Files: *
Copyright: © 2000-2016 Guy St.C. Slater <guy.slater@gmail.com>
License: GPL-3+

Files: debian/*
Copyright: © 2007 Steffen Moeller <moeller@debian.org>
           © 2008 Charles Plessy <plessy@debian.org>
           © 2011-2019 Andreas Tille <tille@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'
